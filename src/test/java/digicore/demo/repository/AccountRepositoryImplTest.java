package digicore.demo.repository;

import digicore.demo.model.Account;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Slf4j

public class AccountRepositoryImplTest {

    AccountRepositoryImpl accountRepositoryImpl;

    private Account account;

    @BeforeEach
    public void setAccount(){
        accountRepositoryImpl = new AccountRepositoryImpl();
        account = new Account();
    }

    @Test
    public void assertThatAccountClassIsNotNull(){
        Assertions.assertNotNull(account);
    }

    @Test
    public void test_createAccount(){

        account.setAccountNumber(0);
        account.setAccountName("Okoroafor Kelechi Divine");
        account.setTransactioinDate(LocalDateTime.now());
        account.setBalance(23.3);

        assertThat(account).isNotNull();
        log.info("Created account successfully -> {}", account);
    }

    @Test
    public void test_disableAccount(){
        AssertionsForClassTypes.assertThat(accountRepositoryImpl.findAccountByAccountNumber(987));
        accountRepositoryImpl.deleteAccount(987);
        log.info("Deleted account successfully -> {}", account);
    }

    @Test
    public void test_updateAccount(){
        accountRepositoryImpl.findAccountByAccountNumber(01);
        AssertionsForClassTypes.assertThat(account).isNotNull();
        account.setAccountName("lovemi");
        account.setAccountNumber(11);
        accountRepositoryImpl.updateAccount(account);
        log.info("Updated account successfully -> {}", account);
    }

    @Test
    public void test_findAccountByPhoneNumber(){
        accountRepositoryImpl.findAccountByAccountNumber(0);
        AssertionsForClassTypes.assertThat(account).isNotNull();
        log.info("An account match that phone number -> {}", account);
    }

    @Test
    public void test_readAllAccount(){
        List<Account> accountList = accountRepositoryImpl.findAllAccounts();
        AssertionsForClassTypes.assertThat(accountList).isNotNull();
        log.info("All accounts -> {}", accountList);
    }
}
package digicore.demo.model;

import digicore.demo.TransactionType.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Transaction {

    private Date transactionDate;
    private TransactionType transactionType;
    private String narrator;
    private Double amount;
    private Double accountBalance;
}

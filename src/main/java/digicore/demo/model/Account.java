package digicore.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Account {

    @NonNull
    private Integer accountNumber;

    @NonNull
    private String accountName;

    @NonNull
    private LocalDateTime transactioinDate;

    @NonNull
    private Double balance;
}

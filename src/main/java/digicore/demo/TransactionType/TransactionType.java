package digicore.demo.TransactionType;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}

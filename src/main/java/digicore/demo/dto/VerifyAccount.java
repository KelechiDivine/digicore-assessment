package digicore.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class VerifyAccount {

    @NonNull
    private String accountNumber;

    @NonNull
    private String accountName;

    @NonNull
    private Double balance;
}

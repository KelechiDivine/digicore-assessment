package digicore.demo.repository;

import digicore.demo.model.Account;
import digicore.demo.model.Transaction;

import java.util.List;

public interface TransactionRepository {

    void CreateTransaction(Transaction transaction);

    List<Transaction> findAllTransactions();

    void deleteTransaction(String tranId);

    void updateTransaction(Transaction transaction);

    Transaction findTransactionByTransactionDate(String tranDate);

    void save(Transaction transaction);


    boolean transactionDoesNotExist(String accountNumber);
}

package digicore.demo.repository;

import digicore.demo.model.Account;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    List<Account> findAllAccounts();

    void deleteAllAccounts();
   void deleteAccount(Account account);

    void updateAccount(Account account);
    Optional<Account> findAccountByAccountNumber(Integer accountNumber);

    Account save(Account account);
}

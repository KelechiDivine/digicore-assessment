package digicore.demo.repository;

import digicore.demo.model.Account;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j

public class AccountRepositoryImpl implements AccountRepository{

    private final Map<Integer, Account> accountRepository = new HashMap<>();

    @Override
    public List<Account> findAllAccounts() {
        List<Account> allAccounts =new ArrayList<>();
        Set<Integer> keys = accountRepository.keySet();
        for (Integer key : keys){
            allAccounts.add(accountRepository.get(key));
        }
        return allAccounts;
    }

    @Override
    public void deleteAllAccounts() {
        accountRepository.clear();
    }

    @Override
    public void deleteAccount(Account account) {

    }

//    @Override
//    public void deleteAccount(Integer id) {
//        accountRepository.remove(id);
//    }

    @Override
    public void updateAccount(Account account) {

    }

    @Override
    public Optional<Account> findAccountByAccountNumber(Integer accountNumber) {
        if (accountRepository.containsKey(accountNumber)){
            log.info("");
        }
        return Optional.of(accountRepository.get(accountNumber));
    }

    @Override
    public Account save(Account account) {

        Integer accountId = null;

        if (account.getAccountNumber() == null){
            accountId = accountRepository.size() + 1;
            account.setAccountNumber(accountId);
        }

        accountId = account.getAccountNumber();
        accountRepository.put(accountId, account);
        return accountRepository.get(accountId);
    }
}

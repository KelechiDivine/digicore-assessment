package digicore.demo.utils.mapper;

import digicore.demo.controller.response.AccountResponse;
import digicore.demo.controller.response.TransactionResponse;
import digicore.demo.model.Account;
import digicore.demo.model.Transaction;

public class Mapper {

    public static AccountResponse map(Account account) {
        AccountResponse account1 = new AccountResponse();
        account1.setAccount(account);
        account1.setMessage(account1.getMessage());
        account1.setResponseCode(account1.getResponseCode());
        account1.setStatus(account1.getStatus());
        account1.setSuccess(account1.isSuccess());
        account1.setTimestamp(account1.getTimestamp());
        return account1;
    }

    public static TransactionResponse map(Transaction transaction){
        TransactionResponse transactionResponse = new TransactionResponse();
        transactionResponse.setTransactionDate(transactionResponse.getTransactionDate());
        transactionResponse.setTransactionType(transactionResponse.getTransactionType());
        transactionResponse.setAccountBalance(transaction.getAccountBalance());
        transactionResponse.setAmount(transactionResponse.getAmount());
        transactionResponse.setNaration(transactionResponse.getNaration());
        return transactionResponse;
    }

    public static Account map(AccountResponse account) {
        return account.getAccount();
    }
}

package digicore.demo.service;

import digicore.demo.exception.AccountException;
import digicore.demo.model.Account;
import digicore.demo.model.Transaction;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Slf4j

public class TransactionServiceImpl implements TransactionService{

    HashMap<Transaction, Date> map = new HashMap<>();

    private Transaction transaction;

    @Override
    public void createTransaction(Transaction transaction) {
        map.put(transaction, transaction.getTransactionDate());

    }

    @Override
    public List<Transaction> findAllTransaction() {
        return null;
    }

    @Override
    public void deleteTransaction(String tranId) {

    }

    @Override
    public void updateTransaction(Transaction transaction) {

    }

    @Override
    public boolean findTransactionByTransactionDate(Date tranDate) {
        return map.containsKey(tranDate);
    }

    Optional<Transaction> transactionOptional;
}

package digicore.demo.service;

import digicore.demo.controller.response.AccountResponse;
import digicore.demo.model.Account;
import digicore.demo.model.Transaction;
import digicore.demo.repository.AccountRepository;
import digicore.demo.repository.TransactionRepositoryImpl;
import digicore.demo.utils.mapper.Mapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service

public class AccountServiceImpl implements AccountService{
    HashMap<Account, String> map = new HashMap<>();

    private AccountRepository accountRepository;
    private AccountServiceImpl accountService;

    private TransactionRepositoryImpl transactionRepositoryImpl;

    private TransactionServiceImpl transactionServiceImpl;
    private Account account;
    private Mapper accountResponse;

    @Override
    public AccountResponse CreateAccount(AccountResponse account) {
        String accountOptional = accountService.findAccountByAccountNumber(account.getResponseCode());

        Account account1 = Mapper.map(account);
        Account account2 = accountRepository.save(account1);

        Transaction transaction = new Transaction();
        transaction.setTransactionDate(transaction.getTransactionDate());
        transaction.setTransactionType(transaction.getTransactionType());
        transaction.setAccountBalance(transaction.getAccountBalance());
        transaction.setAmount(transaction.getAmount());
        transaction.setNarrator(transaction.getNarrator());

        transactionRepositoryImpl.save(transaction);
        return Mapper.map(account2);
    }

    @Override
    public List<Account> findAllAccounts() {
        return accountRepository.findAllAccounts();
    }

    @Override
    public void disableAccount(Integer accountId) {
        map.clear();
    }

    @Override
    public void updateAccount(Account account) {
       accountRepository.save(account);
    }

    @Override
    public String findAccountByAccountNumber(Integer accountNumber) {
        return map.get(accountNumber);
    }

}

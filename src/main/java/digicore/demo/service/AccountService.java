package digicore.demo.service;

import digicore.demo.controller.response.AccountResponse;
import digicore.demo.model.Account;

import java.util.List;

public interface AccountService {

    AccountResponse CreateAccount(AccountResponse account);

    List<Account> findAllAccounts();

    void disableAccount(Integer accountNumber);

    void updateAccount(Account account);
    Object findAccountByAccountNumber(Integer accountNumber);

}

package digicore.demo.service;

import digicore.demo.model.Transaction;

import java.util.Date;
import java.util.List;

public interface TransactionService {

    void createTransaction(Transaction transaction);

    List<Transaction> findAllTransaction();

    void deleteTransaction(String tranId);

    void updateTransaction(Transaction transaction);

    boolean findTransactionByTransactionDate(Date tranDate);
}

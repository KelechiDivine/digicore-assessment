package digicore.demo.controller;

import digicore.demo.TransactionType.TransactionType;
import digicore.demo.controller.response.AccountResponse;
import digicore.demo.controller.response.TransactionResponse;
import digicore.demo.exception.AccountException;
import digicore.demo.model.Account;
import digicore.demo.model.Transaction;
import digicore.demo.repository.AccountRepositoryImpl;
import digicore.demo.service.AccountServiceImpl;
import digicore.demo.service.TransactionServiceImpl;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequestMapping("")
@Slf4j

public class AccountController {

    private AccountServiceImpl accountServiceImpl;

    private TransactionServiceImpl transactionService;

    private AccountRepositoryImpl accountRepositoryImpl;

    private Account account;

    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> queryAccountBalanceByAccountNumberAndPassword(@PathVariable Integer accountNumber) throws AccountException {
        if (accountNumber == null) {
            throw new AccountException("Account number must not be null");
        }

        AccountResponse accountResponse = new AccountResponse(200, true, LocalDateTime.now(), "queried", "success", account);
        return new ResponseEntity<>(accountResponse, HttpStatus.OK);

    }

    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> getAccountstatementTransaction(@PathVariable Integer accountNumber) throws AccountException {

        Transaction transaction = new Transaction();
        if (accountNumber == null) {
            throw new AccountException("Transaction id must not be null");
        }

        if (transactionService.findTransactionByTransactionDate(transaction.getTransactionDate())) {
            throw new AccountException("Transaction does not exist");
        }
        TransactionResponse transactionResponse = new TransactionResponse(LocalDateTime.now(), TransactionType.WITHDRAWAL, "for light", account.getBalance(), account.getBalance());
        return new ResponseEntity<>(transactionResponse, HttpStatus.OK);
    }

    @PostMapping("/deposit/{accountNumber}/{amount}")
    public ResponseEntity<?> deposit(@PathVariable Integer accountNumber, @PathVariable Double amount) throws AccountException {

        if (account.getBalance() < amount) {
            throw new AccountException("You can't make this transaction because you ave little monry.");
        }

        if (!accountNumber.equals(accountNumber)) {
            throw new AccountException("Account number can't be empty.");
        }

        Optional<Account> account = accountRepositoryImpl.findAccountByAccountNumber(accountNumber);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping("/withdrawal/{accountNumber}/{accountPassword}/{withdrawnAmount}")
    public ResponseEntity<?> withdrawProvided(@PathVariable Integer accountNumber, @PathVariable String accountPassword, @PathVariable double withdrawnAmount) throws AccountException {
        if (account.getBalance() < withdrawnAmount) {
            throw new AccountException("Insufficient balance");
        }
        for (int bal = 10; bal <= 1000000; bal++) {
            if (account.getBalance() > 1000000 && account.getBalance() < 10) {
                log.info("");
            }
        }
        AccountResponse accountResponse = new AccountResponse(200, true, LocalDateTime.now(), "authorize", "success", account);
        return new ResponseEntity<>(accountResponse, HttpStatus.OK);
    }
}
package digicore.demo.controller.response;

import digicore.demo.TransactionType.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.time.LocalDateTime;

@Data

@AllArgsConstructor
@NoArgsConstructor

public class TransactionResponse {

    private LocalDateTime transactionDate;
    private TransactionType transactionType;
    private String naration;
    private Double amount;
    private Double accountBalance;
}

package digicore.demo.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import digicore.demo.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class AccountResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private int responseCode;
    private boolean success = true;
    private LocalDateTime timestamp;
    private String message;
    private String status;
    private Account account;
}

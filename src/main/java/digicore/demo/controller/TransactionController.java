package digicore.demo.controller;

import digicore.demo.controller.response.TransactionResponse;
import digicore.demo.exception.AccountException;
import digicore.demo.model.Transaction;
import digicore.demo.repository.TransactionRepositoryImpl;
import digicore.demo.service.TransactionServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("")
@Slf4j

public class TransactionController {

    private TransactionRepositoryImpl transactionRepositoryImpl;
    private TransactionServiceImpl transactionServiceImpl;

    private Transaction transaction;

    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> queryTransaction(@PathVariable String accountNumber) throws AccountException{
        if (accountNumber == null){
            throw new AccountException("Account number is null, please pass your valid number");
        }
        if (transactionRepositoryImpl.transactionDoesNotExist(accountNumber)) {
            throw new AccountException("No transaction was made.");
        }

        TransactionResponse transactionResponse = new TransactionResponse(LocalDateTime.now(), transaction.getTransactionType(),"", transaction.getAccountBalance(), transaction.getAccountBalance());

        return new ResponseEntity<>(transactionResponse, HttpStatus.OK);
    }
}
